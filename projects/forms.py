# Feature14: creating
from django.forms import ModelForm
from projects.models import Project


# Feature14: adding for create_project
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
