# Feature 5: Creating file
from django.urls import path

# Feature 5: adding Project
# Feature 13: adding show_project
# Feature 14: adding create_project
from projects.views import list_projects, show_project, create_project


# Feature X:
# from .views import list_projects


urlpatterns = [
    # Feature 5: adding Project
    # Feature 6: had to rename "home" for the test trick.
    path("", list_projects, name="list_projects"),
    # Feature 13: adding show_project
    path("<int:id>/", show_project, name="show_project"),
    # Feature 14: adding create_project
    path("create/", create_project, name="create_project"),
]

# http://localhost:8000/admin/
# http://localhost:8000/projects/ is our base for all addys right now
# http://localhost:8000/ is redirected up ^
# http://localhost:8000/projects/1/ should be first
# http://localhost:8000/projects/create/
