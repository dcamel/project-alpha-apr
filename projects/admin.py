from django.contrib import admin

# Feature 4: Adding first Project model
from projects.models import Project

# Register your models here.


# Feature 4: Adding first Project model
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "owner",
    ]
