# Feature13: adding get_object_or_404 from show_project
# Feature14: adding redirect from create_project
from django.shortcuts import render, get_object_or_404, redirect

# Feature 5: listview for Project
from projects.models import Project

# Feature 8: protecting list view
from django.contrib.auth.decorators import login_required

# Feature14: adding form create_project
from projects.forms import ProjectForm


# Create your views here.
# Feature 5: listview for Project
# Feature 8: protecting list view
@login_required
def list_projects(request):
    # projectlists = Project.objects.all()
    projectlists = Project.objects.filter(owner=request.user)
    context = {
        "ProjectList": projectlists,
    }
    return render(request, "projects/list.html", context)


# Feature 13: detail view project
@login_required
def show_project(request, id):
    projectshows = get_object_or_404(Project, id=id)
    # tasks = projectshows.tasks.all()
    # projectlistsx = Project.objects.filter(owner=request.user)
    # projectlists = Project.objects.all()
    context = {
        "ProjectTask": projectshows,
    }
    return render(request, "projects/detail.html", context)


# Feature 14: create view
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
