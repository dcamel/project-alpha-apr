from django.db import models

# Feature 3: adding for Project
from django.conf import settings


# Create your models here.


# Feature 3: first project model
# "projects" for foreign key
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(default="")
    #   Python manual doesn't say it needs anything?
    #   yet I keep getting no database entry errors?
    #   Trying empty string
    #   it seems to have worked.  Makes sense, its like Python problems from class.
    #   Need to put _something_ in first?
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
