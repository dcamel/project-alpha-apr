# Feature 7: creating accounts/Login
from django import forms


# Feature 7: creating accounts/Login
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


# Feature10: signup
class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


# Feature 7: creating
# Create a Django form that has username and password attributes,
# both of type CharField with max lengths of 150.
# Make sure the password attribute uses the PasswordInput widget.


# Feature10: signup
# Create a signup form with the following fields:
# username that has a max length of 150 characters
# password that has a max length of 150 characters and uses a PasswordInput
# password_confirmation that has a max length of 150 characters
# and uses a PasswordInput
