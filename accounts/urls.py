# Feature 7: creating for login
from django.urls import path

# Feature 7: adding user_login
# Feature 8: adding user_logout
# Feature 10: adding signup
from accounts.views import user_login, user_logout, user_signup


# always make sure you don't define functions called
# "signup", "login", and "logout"
# they are already in use by django and will mess up


urlpatterns = [
    # Feature 7: adding login
    path("login/", user_login, name="login"),
    # Feature 8: adding user_logout
    path("logout/", user_logout, name="logout"),
    # Feature 10: adding signup
    path("signup/", user_signup, name="signup"),
]


# Feature 7: adding login
# Register the view in the with the path "login/" and the name "login".
# Feature 8: adding user_logout
# Register that view in your urlpatterns list with the path "logout/"
#   and the name "logout"


# http://localhost:8000/accounts/login/
# http://localhost:8000/accounts/signup/
