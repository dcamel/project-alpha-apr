"""
URL configuration for tracker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

# Feature 5: adding Project
from django.urls import path, include

# Feature 6: adding redirect
from django.shortcuts import redirect


# Feature 6: Redirect, "project_list" was from app-urls.py main page
# Feature 6: had to rename "home" for the test trick.
def redirect_to_projectlist(request):
    return redirect("list_projects")


urlpatterns = [
    path("admin/", admin.site.urls),
    # Feature 5: adding Project
    path("projects/", include("projects.urls")),
    # Feature 6: adding redirect from http://localhost:8000/
    # Feature 6: test wants "home" here
    path("", redirect_to_projectlist, name="home"),
    # Feature 7: adding /accounts/ app
    path("accounts/", include("accounts.urls")),
    # Feature 15: adding create_task
    path("tasks/", include("tasks.urls")),
]
