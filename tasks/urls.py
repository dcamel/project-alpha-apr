# Feature 15: Creating file
from django.urls import path

# Feature 15: adding create_task
# Feature 16: adding show_my_tasks
from tasks.views import create_task, show_my_tasks


urlpatterns = [
    # Feature 15: adding create_project
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
