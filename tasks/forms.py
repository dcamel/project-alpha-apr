# Feature15: creating
from django.forms import ModelForm
from tasks.models import Task


# Feature15: adding for create_task
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
