from django.shortcuts import render, redirect

# Feature15: adding for create_task
from django.contrib.auth.decorators import login_required

# Feature15: adding for create_task
from tasks.forms import TaskForm

# Feature16: adding for show_my_tasks
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            # scrumptious recipes/views.py -> edit_recipe
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "MyTaskList": tasks,
    }
    return render(request, "tasks/mytasks.html", context)
