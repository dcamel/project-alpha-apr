from django.contrib import admin

# Feature 12: registering Task model
from tasks.models import Task


# Register your models here.
# Feature 12: registering Task model
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    ]
